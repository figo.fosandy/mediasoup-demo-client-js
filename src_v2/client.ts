import {
  mediaConstraints,
  SignalingClientType,
  SignalingTransportPurposeType,
} from "./enum";
import BaseLogger from "./logger";
import Signaling from "./signaling";
const mediasoupClient = require("mediasoup-client");

export default class WebRtcClient extends BaseLogger {
  signaling: Signaling;
  localStream?: MediaStream;
  device?: typeof mediasoupClient.Device;
  sendTransport?: typeof mediasoupClient.types.Transport;
  recvTransport?: typeof mediasoupClient.types.Transport;
  mapConsumer: Map<
    typeof mediasoupClient.types.MediaKind,
    typeof mediasoupClient.types.Consumer
  >;

  constructor(ip: string) {
    super();
    this.signaling = new Signaling(ip);
    this.mapConsumer = new Map<
      typeof mediasoupClient.types.MediaKind,
      typeof mediasoupClient.types.Consumer
    >();
  }

  async init(clientType: SignalingClientType, name: string): Promise<void> {
    this.signaling.join(clientType, name);
    this.localStream = await navigator.mediaDevices.getUserMedia(
      mediaConstraints
    );
    (document.getElementById(
      "local_video"
    ) as any).srcObject = this.localStream;
    if (clientType === SignalingClientType.agent) {
      this.signaling.onOffer((_) => {
        const confirmed = confirm("Someone answer?\ncall");
        if (confirmed) {
          this.initiateCall();
        }
      });
    }
    this.signaling.onNewConsumer(
      async ({ id, producerId, kind, rtpParameters }, callback) => {
        const consumer = await this.recvTransport.consume({
          id,
          producerId,
          kind,
          rtpParameters,
        });
        this.mapConsumer.set(kind, consumer);

        consumer.on("transportclose", () => {
          this.mapConsumer.delete(kind);
        });

        if (kind === mediasoupClient.types.MediaKind.video) {
          (document.getElementById(
            "received_video"
          ) as any).srcObject = new MediaStream([consumer.track]);
        }
        callback({});
      }
    );
  }

  async initiateCall(): Promise<void> {
    this.device = new mediasoupClient.Device();
    const routerRtpCapabilities = await this.signaling.getRouterRtpCapabilities();
    console.log({ routerRtpCapabilities });
    this.device.load({ routerRtpCapabilities });
    const webRtcProduceTransport = await this.signaling.createWebRtcTransport(
      SignalingTransportPurposeType.produce
    );
    console.log({ webRtcProduceTransport });
    this.sendTransport = this.device.createSendTransport({
      id: webRtcProduceTransport.id,
      iceParameters: webRtcProduceTransport.iceParameters,
      iceCandidates: webRtcProduceTransport.iceCandidates,
      dtlsParameters: webRtcProduceTransport.dtlsParameters,
      sctpParameters: webRtcProduceTransport.sctpParameters,
    });
    console.log({ sendTransport: this.sendTransport });
    this.sendTransport.on(
      "connect",
      async ({ dtlsParameters }, callback, errback) => {
        try {
          console.log({ dtlsParameters });
          await this.signaling.connectWebRtcTransport(
            SignalingTransportPurposeType.produce,
            this.sendTransport.id,
            dtlsParameters
          );
          callback();
        } catch (error) {
          errback(error);
        }
      }
    );
    this.sendTransport.on(
      "produce",
      async ({ kind, rtpParameters }, callback, errback) => {
        try {
          console.log({ kind, rtpParameters });
          await this.signaling.produceWebRtcTransport(
            this.sendTransport.id,
            kind,
            rtpParameters
          );
          callback();
        } catch (error) {
          errback(error);
        }
      }
    );
    const webRtcConsumeTransport = await this.signaling.createWebRtcTransport(
      SignalingTransportPurposeType.consume
    );
    console.log({ webRtcConsumeTransport });
    this.recvTransport = this.device.createRecvTransport({
      id: webRtcConsumeTransport.id,
      iceParameters: webRtcConsumeTransport.iceParameters,
      iceCandidates: webRtcConsumeTransport.iceCandidates,
      dtlsParameters: webRtcConsumeTransport.dtlsParameters,
      sctpParameters: webRtcConsumeTransport.sctpParameters,
    });
    console.log({ recvTransport: this.recvTransport });
    this.recvTransport.on(
      "connect",
      async ({ dtlsParameters }, callback, errback) => {
        try {
          console.log({ dtlsParameters });
          await this.signaling.connectWebRtcTransport(
            SignalingTransportPurposeType.consume,
            this.recvTransport.id,
            dtlsParameters
          );
          callback();
        } catch (error) {
          errback(error);
        }
      }
    );
    this.signaling.emitJoiningCall({
      rtpCapabilities: this.device.rtpCapabilities,
    });
  }

  async offer(): Promise<void> {
    console.log("called");
    await this.signaling.offer();
    await this.initiateCall();
  }
}
