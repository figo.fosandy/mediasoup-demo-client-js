import WebRtcClient from "./client";
import { SignalingClientType } from "./enum";

console.log("Hello World from your main file!");

window.onload = async () => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const clientType = urlParams.get("clientType");
  const name = urlParams.get("name") || "anonymous";
  console.log(`${name} - ${clientType}`);
  if (
    Object.values(SignalingClientType).indexOf(
      clientType as SignalingClientType
    ) !== -1
  ) {
    const webRtcService = new WebRtcClient("http://192.168.0.100:8080");
    await webRtcService.init(clientType as SignalingClientType, name);

    if (clientType === SignalingClientType.customer) {
      const button = document.createElement("button");
      button.innerHTML = "Offer";
      button.onclick = () => {
        button.innerHTML = "Hang up (not supported yet)";
        button.disabled = true;
        webRtcService.offer();
      };
      button.id = "hangup-button";
      document.getElementById("idk").appendChild(button);
    }
  }
};
