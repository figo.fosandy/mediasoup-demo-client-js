export const mediaConstraints = {
  audio: false,
  video: true,
};

export enum SignalingClientType {
  agent = "agent",
  customer = "customer",
}

export enum SignalingClientEventType {
  join = "join",
  offer = "offer",
  answer = "answer",
  icecandidate = "icecandidate",
  getRouterRtpCapabilities = "getRouterRtpCapabilities",
  createWebRtcTransport = "createWebRtcTransport",
  connectWebRtcTransport = "connectWebRtcTransport",
  produceWebRtcTransport = "produceWebRtcTransport",
  joiningCall = "joiningCall",
}

export enum SignalingServerEventType {
  offer = "offer",
  answer = "answer",
  icecandidate = "icecandidate",
  routerRtpCapabilities = "routerRtpCapabilities",
  newConsumer = "newConsumer",
}

export enum SignalingTransportPurposeType {
  consume = "consume",
  produce = "produce",
}
