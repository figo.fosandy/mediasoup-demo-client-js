import io from "socket.io-client";
import {
  SignalingClientEventType,
  SignalingClientType,
  SignalingServerEventType,
  SignalingTransportPurposeType,
} from "./enum";
import BaseLogger from "./logger";
const mediasoupclient = require("mediasoup-client");

export default class Signaling extends BaseLogger {
  socket: SocketIOClient.Socket;

  constructor(ip: string) {
    super();
    this.socket = io(ip, {
      transports: ["websocket"],
    });
  }

  emitEvent(eventType: SignalingClientEventType, data: unknown): void {
    this.log(`[emitEvent] ${JSON.stringify({ eventType, data })}`);
    this.socket.emit(eventType, JSON.stringify(data));
  }

  async emitEventQuestion(
    eventType: SignalingClientEventType,
    data: unknown
  ): Promise<any> {
    this.log(`[emitEventQuestion] ${JSON.stringify({ eventType, data })}`);
    return new Promise((resolve) =>
      this.socket.emit(eventType, JSON.stringify(data), resolve)
    );
  }

  onEvent(
    eventType: SignalingServerEventType,
    onEventData: (data: unknown) => void
  ): void {
    this.socket.on(eventType, (data: string) => {
      this.log(`[onEvent] ${JSON.stringify({ eventType, data })}`);
      onEventData(JSON.parse(data));
    });
  }

  onEventWithAck(
    eventType: SignalingServerEventType,
    onEventData: (data: any, callback: (data: unknown) => void) => void
  ): void {
    this.socket.on(eventType, (data: string, callback) => {
      this.log(`[onEventWithAck] ${JSON.stringify({ eventType, data })}`);
      onEventData(JSON.parse(data), callback);
    });
  }

  join(clientType: SignalingClientType, name: string) {
    this.log("connected");
    this.emitEvent(SignalingClientEventType.join, {
      clientType: clientType,
      name: name,
    });
  }

  async getRouterRtpCapabilities(): Promise<
    typeof mediasoupclient.types.RtpCapabilities
  > {
    return this.emitEventQuestion(
      SignalingClientEventType.getRouterRtpCapabilities,
      {}
    );
  }

  async connectWebRtcTransport(
    type: SignalingTransportPurposeType,
    id: string,
    dtlsParameters: typeof mediasoupclient.types.DtlsParameters
  ): Promise<typeof mediasoupclient.types.TransportOptions> {
    return this.emitEventQuestion(
      SignalingClientEventType.connectWebRtcTransport,
      { type, id, dtlsParameters }
    );
  }

  async createWebRtcTransport(
    type: SignalingTransportPurposeType
  ): Promise<typeof mediasoupclient.types.TransportOptions> {
    return this.emitEventQuestion(
      SignalingClientEventType.createWebRtcTransport,
      { type }
    );
  }

  async produceWebRtcTransport(
    id: string,
    kind: typeof mediasoupclient.types.MediaKind,
    rtpParameters: typeof mediasoupclient.types.RtpParameters
  ): Promise<{ id: string }> {
    return this.emitEventQuestion(
      SignalingClientEventType.produceWebRtcTransport,
      { id, kind, rtpParameters }
    );
  }

  async offer(): Promise<{ id: string }> {
    return this.emitEventQuestion(SignalingClientEventType.offer, {});
  }

  onOffer(onEventData: (data: unknown) => void) {
    this.onEvent(SignalingServerEventType.offer, onEventData);
  }

  onNewConsumer(
    onEventData: (
      data: {
        id: string;
        producerId: string;
        kind: typeof mediasoupclient.types.MediaKind;
        rtpParameters: typeof mediasoupclient.types.RtpParameters;
        clientId: string;
      },
      callback: (data: unknown) => void
    ) => void
  ) {
    this.onEventWithAck(SignalingServerEventType.newConsumer, onEventData);
  }

  emitJoiningCall(data: unknown) {
    this.emitEvent(SignalingClientEventType.joiningCall, data);
  }
}
