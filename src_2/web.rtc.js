import SignalingClientService from "./signaling.client";
import { mediaConstraints } from "./constant";

export default class WebRtcService {
  constructor(ip) {
    this.signaling = new SignalingClientService(ip);
    this.pc = new RTCPeerConnection({
      iceServers: [
        { url: "stun:stun.l.google.com:19302" },
        {
          url: "turn:35.184.112.236:3478",
          username: "username",
          credential: "password",
        },
      ],
    });
  }

  async init(clientType, name) {
    console.log(this.pc);
    this.localStream = await navigator.mediaDevices.getUserMedia(
      mediaConstraints
    );
    document.getElementById("local_video").srcObject = this.localStream;
    this.localStream
      .getTracks()
      .forEach((track) => this.pc.addTrack(track, this.localStream));
    this.pc.ontrack = (event) => {
      document.getElementById("received_video").srcObject = event.streams[0];
    };
    this.signaling.join(clientType, name);
    this.signaling.onIcecandidate((icecandidate) =>
      this.pc.addIceCandidate(icecandidate)
    );
    this.signaling.onOffer(async (offer) => {
      const confirmed = confirm("Someone offer\nAnswer?");
      if (confirmed) {
        this.pc.setRemoteDescription(offer);
        const answer = await this.pc.createAnswer();
        this.signaling.answer(answer);
        this.pc.setLocalDescription(answer);
      }
    });
    this.signaling.onAnswer(async (answer) => {
      this.pc.setRemoteDescription(answer);
    });
    this.pc.oniceconnectionstatechange = console.log;
    this.pc.onicegatheringstatechange = console.log;
    this.pc.onicecandidate = (event) => {
      if (event.candidate === null) {
        console.log("icecandidate done");
        return;
      }
      this.signaling.icecandidate(event.candidate);
    };
  }

  async createOffer() {
    const offer = await this.pc.createOffer();
    this.pc.setLocalDescription(offer);
    this.signaling.offer(offer);
  }
}
