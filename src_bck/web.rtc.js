import SignalingClientService from "./signaling.client";
import { mediaConstraints } from "./constant";
import mediasoupClient from "mediasoup-client";

export default class WebRtcService {
  constructor(ip) {
    this.signaling = new SignalingClientService(ip);
  }

  async init(clientType, name) {
    this.localStream = await navigator.mediaDevices.getUserMedia(
      mediaConstraints
    );
    document.getElementById("local_video").srcObject = this.localStream;
  }

  async createOffer() {}
}
