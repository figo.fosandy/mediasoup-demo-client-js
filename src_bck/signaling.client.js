import io from "socket.io-client";
import { SignalingClientEventType, SignalingServerEventType } from "./constant";

export default class SignalingClientService {
  constructor(ip) {
    this.socket = io(ip, {
      transports: ["websocket"],
    });
  }

  log(message) {
    console.log(`[${this.constructor.name}] ${message}`);
  }

  emitEvent(eventType, data) {
    const payload = JSON.stringify(data);
    console.log(`[emitEvent] ${eventType}: ${payload}`);
    this.socket.emit(eventType, payload);
  }

  async emitEventQuestion(eventType, data) {
    return await new Promise((resolve) => {
      const payload = JSON.stringify(data);
      console.log(`[emitEventQuestion] ${eventType}: ${payload}`);
      this.socket.emit(eventType, payload, resolve);
    });
  }

  onEvent(eventType, onEventData) {
    this.socket.on(eventType, (data) => {
      console.log(`[onEvent] ${eventType}: ${data}`);
      onEventData(JSON.parse(data));
    });
  }

  join(clientType, name) {
    this.log("connected");
    this.emitEvent(SignalingClientEventType.join, {
      clientType: clientType,
      name: name,
    });
  }

  offer(offer) {
    this.emitEvent(SignalingClientEventType.offer, offer);
  }

  answer(answer) {
    this.emitEvent(SignalingClientEventType.answer, answer);
  }

  icecandidate(icecandidate) {
    this.emitEvent(SignalingClientEventType.icecandidate, icecandidate);
  }

  onOffer(onEventData) {
    this.onEvent(SignalingServerEventType.offer, onEventData);
  }

  onAnswer(onEventData) {
    this.onEvent(SignalingServerEventType.answer, onEventData);
  }

  onIcecandidate(onEventData) {
    this.onEvent(SignalingServerEventType.icecandidate, onEventData);
  }
}
