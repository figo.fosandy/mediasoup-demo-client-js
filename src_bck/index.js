import { SignalingClientType } from "./constant";
import WebRtcService from "./web.rtc";

console.log("Hello World from your main file!");

window.onload = () => {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const clientType = urlParams.get("clientType");
  const name = urlParams.get("name") || "anonymous";
  console.log(`${name} - ${clientType}`);
  if (Object.values(SignalingClientType).indexOf(clientType) !== -1) {
    const webRtcService = new WebRtcService("http://192.168.0.100:8080");
    webRtcService.init(clientType, name);

    if (clientType === SignalingClientType.customer) {
      const button = document.createElement("button");
      button.innerHTML = "Offer";
      button.onclick = () => {
        button.innerHTML = "Hang up (not supported yet)";
        button.disabled = true;
        webRtcService.createOffer();
      };
      button.id = "hangup-button";
      document.getElementById("idk").appendChild(button);
    }
  }
};
