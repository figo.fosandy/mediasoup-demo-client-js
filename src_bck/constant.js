export const mediaConstraints = {
  audio: false,
  video: true,
};

export const SignalingClientType = {
  agent: "agent",
  customer: "customer",
};

export const SignalingClientEventType = {
  join: "join",
  offer: "offer",
  answer: "answer",
  icecandidate: "icecandidate",
};

export const SignalingServerEventType = {
  offer: "offer",
  answer: "answer",
  icecandidate: "icecandidate",
};
