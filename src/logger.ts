export default abstract class BaseLogger {
  tag: string;

  constructor() {
    this.tag = `[${this.constructor.name}]`;
    this.log("created");
  }

  log(message: string): void {
    console.log(`${this.tag} ${message}`);
  }
}
