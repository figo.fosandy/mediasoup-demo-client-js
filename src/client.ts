import {
  mediaConstraints,
  SignalingClientType,
  SignalingTransportPurposeType,
} from "./enum";
import BaseLogger from "./logger";
import Signaling from "./signaling";
import mediasoupClient = require("mediasoup-client");

export default class WebRtcClient extends BaseLogger {
  signaling: Signaling;
  localStream?: MediaStream;
  remoteStream?: MediaStream;
  device?: mediasoupClient.Device;
  sendTransport?: mediasoupClient.types.Transport;
  recvTransport?: mediasoupClient.types.Transport;
  mapConsumer: Map<
    mediasoupClient.types.MediaKind,
    mediasoupClient.types.Consumer
  >;

  constructor(ip: string) {
    super();
    this.signaling = new Signaling(ip);
    this.mapConsumer = new Map<
      mediasoupClient.types.MediaKind,
      mediasoupClient.types.Consumer
    >();
  }

  async init(clientType: SignalingClientType, name: string): Promise<void> {
    this.localStream = await navigator.mediaDevices.getUserMedia(
      mediaConstraints
    );
    (document.getElementById(
      "local_video"
    ) as any).srcObject = this.localStream;
    this.signaling.join(clientType, name);

    if (clientType === SignalingClientType.agent) {
      this.signaling.onOffer((_) => {
        const confirmed = confirm("Someone answer?\ncall");
        if (confirmed) {
          this.initiateCall();
        }
      });
    }

    this.signaling.onNewConsumer(
      async ({ id, producerId, kind, rtpParameters }, callback) => {
        const consumer = await this.recvTransport.consume({
          id,
          producerId,
          kind,
          rtpParameters,
        });
        this.mapConsumer.set(kind, consumer);

        consumer.on("transportclose", () => {
          this.mapConsumer.delete(kind);
        });

        this.remoteStream = this.remoteStream || new MediaStream();

        this.remoteStream.addTrack(consumer.track);

        (document.getElementById(
          "received_video"
        ) as any).srcObject = this.remoteStream;
        callback({});
      }
    );
  }

  async initiateCall(): Promise<void> {
    this.device = new mediasoupClient.Device();

    const routerRtpCapabilities = await this.signaling.getRouterRtpCapabilities();

    console.log({ routerRtpCapabilities });
    await this.device.load({ routerRtpCapabilities });


    const webRtcProduceTransport = await this.signaling.createWebRtcTransport(
      SignalingTransportPurposeType.produce
    );
    console.log({ webRtcProduceTransport });

    this.sendTransport = this.device.createSendTransport({
      id: webRtcProduceTransport.id,
      iceParameters: webRtcProduceTransport.iceParameters,
      iceCandidates: webRtcProduceTransport.iceCandidates,
      dtlsParameters: webRtcProduceTransport.dtlsParameters,
      sctpParameters: webRtcProduceTransport.sctpParameters,
    });
    console.log({ sendTransport: this.sendTransport });

    this.sendTransport.on(
      "connect",
      async ({ dtlsParameters }, callback, errback) => {
        try {
          console.log({ dtlsParameters });
          await this.signaling.connectWebRtcTransport(
            SignalingTransportPurposeType.produce,
            this.sendTransport.id,
            dtlsParameters
          );
          callback();
        } catch (error) {
          errback(error);
        }
      }
    );
    this.sendTransport.on(
      "produce",
      async ({ kind, rtpParameters }, callback, errback) => {
        try {
          console.log({ kind, rtpParameters });
          const id = await this.signaling.produceWebRtcTransport(
            this.sendTransport.id,
            kind,
            rtpParameters
          );
          callback({ id });
        } catch (error) {
          errback(error);
        }
      }
    );
    
    const webRtcConsumeTransport = await this.signaling.createWebRtcTransport(
      SignalingTransportPurposeType.consume
    );
    console.log({ webRtcConsumeTransport });
    this.recvTransport = this.device.createRecvTransport({
      id: webRtcConsumeTransport.id,
      iceParameters: webRtcConsumeTransport.iceParameters,
      iceCandidates: webRtcConsumeTransport.iceCandidates,
      dtlsParameters: webRtcConsumeTransport.dtlsParameters,
      sctpParameters: webRtcConsumeTransport.sctpParameters,
    });
    console.log({ recvTransport: this.recvTransport });
    this.recvTransport.on(
      "connect",
      async ({ dtlsParameters }, callback, errback) => {
        try {
          console.log({ dtlsParameters });
          await this.signaling.connectWebRtcTransport(
            SignalingTransportPurposeType.consume,
            this.recvTransport.id,
            dtlsParameters
          );
          callback();
        } catch (error) {
          errback(error);
        }
      }
    );
    await this.signaling.emitJoiningCall({
      rtpCapabilities: this.device.rtpCapabilities,
    });
    this.sendTransport.produce({
      track: this.localStream.getVideoTracks()[0],
    });
    this.sendTransport.produce({
      track: this.localStream.getAudioTracks()[0],
    });
  }

  async offer(): Promise<void> {
    console.log("called");
    await this.signaling.offer();
    await this.initiateCall();
  }
}
