import io = require("socket.io-client");
import {
  SignalingClientEventType,
  SignalingClientType,
  SignalingServerEventType,
  SignalingTransportPurposeType,
} from "./enum";
import BaseLogger from "./logger";
import mediasoupclient = require("mediasoup-client");

export default class Signaling extends BaseLogger {
  socket: SocketIOClient.Socket;

  constructor(ip: string) {
    super();
    this.socket = io(ip, {
      transports: ["websocket"],
    });
  }

  emitEvent(eventType: SignalingClientEventType, data: unknown): void {
    this.log(`[emitEvent] ${JSON.stringify({ eventType, data })}`);
    this.socket.emit(eventType, data);
  }

  async emitEventWithAck(
    eventType: SignalingClientEventType,
    data?: unknown
  ): Promise<any> {
    return new Promise((resolve) => {
      if (!this.socket.connected) {
        resolve(undefined);
      }
      this.log(`[emitEventWithAck] ${JSON.stringify({ eventType, data })}`);
      this.socket.emit(eventType, data, resolve);
    });
  }

  onEvent(
    eventType: SignalingServerEventType,
    onEventData: (data: unknown) => void
  ): void {
    this.socket.on(eventType, (data) => {
      this.log(`[onEvent] ${JSON.stringify({ eventType, data })}`);
      onEventData(data);
    });
  }

  onEventWithAck(
    eventType: SignalingServerEventType,
    onEventData: (data: any, callback: (data: unknown) => void) => void
  ): void {
    this.socket.on(eventType, (data, callback) => {
      this.log(`[onEventWithAck] ${JSON.stringify({ eventType, data })}`);
      onEventData(data, callback);
    });
  }

  join(clientType: SignalingClientType, name: string) {
    this.log("connected");
    this.emitEvent(SignalingClientEventType.join, {
      clientType: clientType,
      name: name,
    });
  }

  async getRouterRtpCapabilities(): Promise<mediasoupclient.types.RtpCapabilities> {
    return this.emitEventWithAck(
      SignalingClientEventType.getRouterRtpCapabilities
    );
  }

  async connectWebRtcTransport(
    type: SignalingTransportPurposeType,
    id: string,
    dtlsParameters: mediasoupclient.types.DtlsParameters
  ): Promise<mediasoupclient.types.TransportOptions> {
    return this.emitEventWithAck(
      SignalingClientEventType.connectWebRtcTransport,
      { type, id, dtlsParameters }
    );
  }

  async createWebRtcTransport(
    type: SignalingTransportPurposeType
  ): Promise<mediasoupclient.types.TransportOptions> {
    return this.emitEventWithAck(
      SignalingClientEventType.createWebRtcTransport,
      { type }
    );
  }

  async produceWebRtcTransport(
    id: string,
    kind: mediasoupclient.types.MediaKind,
    rtpParameters: mediasoupclient.types.RtpParameters
  ): Promise<{ id: string }> {
    return this.emitEventWithAck(
      SignalingClientEventType.produceWebRtcTransport,
      { id, kind, rtpParameters }
    );
  }

  async offer(): Promise<{ id: string }> {
    return this.emitEventWithAck(SignalingClientEventType.offer);
  }

  onOffer(onEventData: (data: unknown) => void) {
    this.onEvent(SignalingServerEventType.offer, onEventData);
  }

  onNewConsumer(
    onEventData: (
      data: {
        id: string;
        producerId: string;
        kind: mediasoupclient.types.MediaKind;
        rtpParameters: mediasoupclient.types.RtpParameters;
        clientId: string;
      },
      callback: (data: unknown) => void
    ) => void
  ) {
    this.onEventWithAck(SignalingServerEventType.newConsumer, onEventData);
  }

  async emitJoiningCall(data: unknown) {
    return this.emitEventWithAck(SignalingClientEventType.joiningCall, data);
  }
}
